var gulp = require('gulp'),
	serve = require('gulp-serve'),
	runSequence = require('run-sequence'),
	jshint = require('gulp-jshint'),
	historyApiFallback = require('connect-history-api-fallback'),
	browserSync = require('browser-sync');

gulp.task('default', function(callback){
	runSequence('watch', 'serve', callback);
});

// gulp.task('serve', serve('src'));


gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: "src",
      middleware: [ historyApiFallback() ]
    }
  });
});

gulp.task('lint', function(){
	return gulp.src(['!src/vendor/*.js', 'src/**/*.js'])
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('watch', function(){
	gulp.watch('src/**/*.js', ['lint']);
})