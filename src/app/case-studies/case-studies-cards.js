(function(){

'use strict';

angular
	.module('sdApp.caseStudiesView',[])



	.config(['$stateProvider', function($stateProvider){
		$stateProvider.state('case-studies', {
			url: '/case-studies',
			templateUrl: 'app/case-studies/case-studies.tmpl.html',
			controller: 'CaseStudyController'
		});
	}])

	.controller('CaseStudyController', ['$scope',  function($scope){

		$scope.studies = [
			{
				"title": "SPS",
				"region": "Regional Government",
				"cardImage": "https://placekitten.com/g/300/300",
				"cardColour": "rgb(155, 24, 21)"
			},
			{
				"title": "MoJ",
				"region": "Ministry of Justice",
				"cardImage": "https://placekitten.com/g/300/300",
				"cardColour": "rgb(11, 105, 129)"
			},
			{
				"title": "DEFRA",
				"region": "Central Government",
				"cardImage": "https://placekitten.com/g/300/300",
				"cardColour": "rgb(44, 158, 104)"
			},
			{
				"title": "ACCA",
				"region": "Regional Government",
				"cardImage": "https://placekitten.com/g/300/300",
				"cardColour": "rgb(215, 100, 0)"
			},
			{
				"title": "Marine Scotland",
				"region": "Government",
				"cardImage": "https://placekitten.com/g/300/300",
				"cardColour": "rgb(215, 100, 0)"
			}
		];
	}]);
})();