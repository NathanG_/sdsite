(function(){

'use strict';

angular
	.module('ServiceDesign', [
		'ui.router',
		'sdApp.methodView',
		'sdApp.peopleView',
		'sdApp.approachView',
		'sdApp.caseStudiesView',
		'sdApp.homepageView'
	])

	.config(function($urlRouterProvider, $locationProvider){
		$urlRouterProvider.otherwise('/');
		$urlRouterProvider.when('/', '/home');
		
		$locationProvider.html5Mode(true);
	})

	.controller('FirstController', ['$scope', '$location', function($scope, $location){
		$scope.go = function(path){
			$location.path(path);
		};
	}]);
})();