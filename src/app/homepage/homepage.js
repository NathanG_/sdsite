(function(){

'use strict';

angular
	.module('sdApp.homepageView',[])

	.config(['$stateProvider', function($stateProvider){
		$stateProvider.state('home', {
			url: '/home',
			templateUrl: 'app/homepage/homepage-card.tmpl.html',
			controller: 'HomeCardController'
		});
	}])

	.controller('HomeCardController', ['$scope', function($scope){
		$scope.homepageCards = [
			{
				"title": "We build digital experiences", 
				"description": "See our unique way  of solving the challenges our clients face", 
				"buttonText": "Our Approach",
				"buttonNav": "/our-approach", 
				"cardImage": "http://via.placeholder.com/650x400",
				"cardColour": "rgba(225, 75, 15, 0.8)"
			},
			{
				"title": "Scottish Prison Service", 
				"description": "We Developed a Solution Allowing Prison Staff to access any offender record from their tablet device while connected to an internal prison network", 
				"buttonText": "Read More", 
				"buttonNav": "/case-studies",
				"cardImage": "http://via.placeholder.com/650x400",
				"cardColour": "rgba(225, 75, 15, 0.8)"
			},
			{
				"title": "How We Solve Problems", 
				"description": "We use a system of 'Method Cards' to solve any business problem, big or small", 
				"buttonText": "Our Methods",
				"buttonNav": "/methods",
				"cardImage": "http://via.placeholder.com/650x400",
				"cardColour": "rgba(225, 75, 15, 0.8)"
			},
			{
				"title": "Who are we?", 
				"description": "Come and meet our team of experienced service designers, consultants visual designers, and developers with over 150+ years of experience", 
				"buttonText": "Our People",
				"buttonNav": "/people",
				"cardImage": "http://via.placeholder.com/650x400",
				"cardColour": "rgba(225, 75, 15, 0.8)"
			}
		];
		console.log("hello world");
		console.log($scope.homepageCards);
	}]);
})();