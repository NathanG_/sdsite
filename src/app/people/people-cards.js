(function(){

'use strict';

angular
	.module('sdApp.peopleView',[])

	.config(['$stateProvider', function($stateProvider){
		$stateProvider.state('people', {
				url: '/people',
				templateUrl: 'app/people/people.tmpl.html',
				controller: 'PeopleCardController'
			});
	}])

	.controller('PeopleCardController', ['$scope',  function($scope){

		$scope.peopleCards = [
			{
				"title": "Our Team", 
				"description": "The Sopra Steria Service Design Team is a multi-disciplinary group of experienced researchers, designers, consultants and developers, based in Edinburgh, Glasgow and London.", 
				"buttonText": "Meet the Team",
				"buttonNav": "#", 
				"cardImage": "http://via.placeholder.com/650x400",
				"cardColour": "rgba(225, 75, 15, 0.8)"
			},
			{
				"title": "Centre of Excellence", 
				"description": "Our Centre of Excellence is an environment to nurture great leaders within Service Design, who we believe will be the next generation of entrepreneurial leaders contributing to Sopra Steria’s success.", 
				"buttonText": "More", 
				"buttonNav": "#",
				"cardImage": "http://via.placeholder.com/650x400",
				"cardColour": "rgba(225, 75, 15, 0.8)"
			},
			{
				"title": "Roles", 
				"description": "Know the difference between a UX Designer and a Visual Designer? We'll tell you all you need to know about who does what", 
				"buttonText": "More",
				"buttonNav": "#",
				"cardImage": "http://via.placeholder.com/650x400",
				"cardColour": "rgba(225, 75, 15, 0.8)"
			},
			{
				"title": "Graduate Programme", 
				"description": "Our Digital Consulting Graduates will specialise in either User Experience, User Interface or Visual Design.", 
				"buttonText": "Join Us",
				"buttonNav": "#",
				"cardImage": "http://via.placeholder.com/650x400",
				"cardColour": "rgba(225, 75, 15, 0.8)"
			}
		];
	}]);
})();