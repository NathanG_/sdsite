(function(){

'use strict';

angular
	.module('sdApp.getMethodCardsFactory', [])

	.factory('getMethodCardsFactory', function (){
		var getMethodCards = {
			doTask: getMethodCardsFun(100)
		};
		return getMethodCards;

		function getMethodCardsFun(num){
			var tempJSON = [];
			var wp = new WPAPI({ endpoint: 'http://sd.localhost:8000/wp-json' });
			wp.myCustomResource = wp.registerRoute( 'wp/v2', '/method_cards/(?P<per_page>\\d+)' );
			return wp.myCustomResource().perPage(num)
				.then(function(data){
				 	data.forEach(function(ele){
			    		var tempObj = {}; 
			    		
			    		tempObj.title = ele.title.rendered;
						tempObj.imgUrl = ele.acf.method_image.url;
						tempObj.effort = ele.acf.effort;
						tempObj.participants = [];
						tempObj.participants.push.apply(tempObj.participants, ele.acf.participants);
						tempObj.phase = ele.acf.phase;
						tempObj.related_cards = [];
						tempObj.related_cards.push.apply(tempObj.related_cards, ele.acf.related_cards);
						tempObj.what_are_they = ele.acf.what_are_they;
						tempObj.why_use_them = ele.acf.why_use_them;
						tempObj.id = ele.id;
						// console.log(tempObj);
						tempJSON.push(tempObj);	
			     	});
			    	   	return tempJSON;
			 	}).catch(function(err){
					console.log(err);
				});
	    	 
		}
	});
})();