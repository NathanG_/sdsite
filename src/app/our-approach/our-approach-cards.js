(function(){

'use strict';

angular
	.module('sdApp.approachView',[])

	.config(['$stateProvider', function($stateProvider){
		$stateProvider.state('our-approach', {
			url: '/our-approach',
			templateUrl: 'app/our-approach/our-approach.tmpl.html',
			controller: 'OurApproachCardController'
		});		
	}])

	.controller('OurApproachCardController', ['$scope',  function($scope){

		$scope.approachSections = [
			{
				"title": "Strategy",
				"description": "Before you start any journey you should know where you’re going. That’s why we work closely with businesses to plan, execute and measure an effective strategic vision",
				"buttonText": "Read More",
				"cardImage": "http://via.placeholder.com/650x400"
			},
			{
				"title": "Research",
				"description": "A combination of methods from ethnographic research to one on one interviews really help us understand the big challenges your users face",
				"buttonText": "Read More",
				"cardImage": "http://via.placeholder.com/650x400"
			},
			{
				"title": "Design",
				"description": "Once we are confident we know what users need we build the best interactions we can. And we'll test our designs to make sure they are as robust as possible",
				"buttonText": "Read More",
				"cardImage": "http://via.placeholder.com/650x400"
			},
			{
				"title": "Deliver",
				"description": "The final step is to assemble all our expertise into one place and deliver a platform that meets the needs of the user and is capable of facing future challenges",
				"buttonText": "Read More",
				"cardImage": "http://via.placeholder.com/650x400"
			}
		];
	}]);
})();