(function(){

'use strict';

angular
	.module('sdApp.methodView',[
		'sdApp.getMethodCardsFactory'
	])



	.config(['$stateProvider', function($stateProvider, getMethodCardsFactory){
		$stateProvider.state('methods', {
				url: '/methods',
				templateUrl: 'app/methods/methods.tmpl.html',
				controller: 'MethodCardController',
				resolve:{
					getMethods: function(getMethodCardsFactory){
						return getMethodCardsFactory.doTask;
					}	
				}
			});
	}])

	.controller('MethodCardController', ['$scope', 'getMethods',  function($scope, getMethods){
		var methods = [];
		$scope.methods = getMethods;

	}]);
})();
